//
//  BowlingGameViewModel.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 25/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import RxSwift

class BowlingGameViewModel {

    // MARK: Properties

    private let NUMBER_OF_FRAMES = 10
    private let bowlingScoreManager = BowlingScoreManager()
    let resultObservable = Variable("")
    let framesObservable = Variable([Frame]())

    var resultHidden: Bool = true

    var currentThrowSet: [Throw] = [] {
        didSet {
            if let firstElement = currentThrowSet.first {
                if firstElement == .strike && frames.count < NUMBER_OF_FRAMES - 1 {
                    frames.append(Frame(throwSet: currentThrowSet))
                    currentThrowSet = []
                }
                if currentThrowSet.count == 2 {
                    if frames.count == (NUMBER_OF_FRAMES - 1) && (firstElement == .strike || currentThrowSet[1] == .spare) {
                        return
                    } else {
                        let throwSet = currentThrowSet
                        frames.append(Frame(throwSet: throwSet))
                        currentThrowSet = []
                    }
                }
                if currentThrowSet.count == 3 {
                    let throwSet = currentThrowSet
                    frames.append(Frame(throwSet: throwSet))
                    currentThrowSet = []
                }
            }
        }
    }

    var frames: [Frame] = [] {
        didSet {
            framesObservable.value = frames
            if frames.count == NUMBER_OF_FRAMES {
                bowlingGame = BowlingGame(frames: frames)
            }
        }
    }

    var bowlingGame: BowlingGame? {
        didSet {
            if let game = bowlingGame {
                let total = bowlingScoreManager.returnTotal(forGame: game)
                resultHidden = false
                resultObservable.value = String(total)
            }
        }
    }

    // MARK: Methods

    private func reset() {
        resultHidden = true
        currentThrowSet = []
        frames = []
        bowlingGame = nil
        resultObservable.value = ""
    }

    func userDidSelectCellWith(indexPath: IndexPath) {
        let amount = indexPath.row
        if currentThrowSet.isEmpty {
            let t = amount == 10 ? Throw.strike : .numberOfPins(amount)
            currentThrowSet.append(t)
        } else if let firstElement = currentThrowSet.first {
            if frames.count == NUMBER_OF_FRAMES - 1 {
                if currentThrowSet.count == 2 {
                    let t = amount == 10 ? Throw.strike : .numberOfPins(amount)
                    currentThrowSet.append(t)
                } else {
                    switch firstElement {
                    case .strike:
                        let t = amount == 10 ? Throw.strike : .numberOfPins(amount)
                        currentThrowSet.append(t)
                    default:
                        let t = amount == 10 ? Throw.spare : .numberOfPins(amount)
                        let firstAmount = bowlingScoreManager.getTotalPinsFor(firstElement)
                        let secondAmount = bowlingScoreManager.getTotalPinsFor(t)
                        let sum = firstAmount + secondAmount
                        let secondThrow = sum == 10 ? Throw.spare : .numberOfPins(secondAmount)
                        currentThrowSet.append(secondThrow)
                    }
                }
            } else {
                let t = amount == 10 ? Throw.spare : .numberOfPins(amount)
                let firstAmount = bowlingScoreManager.getTotalPinsFor(firstElement)
                let secondAmount = bowlingScoreManager.getTotalPinsFor(t)
                let sum = firstAmount + secondAmount
                let finalThrow = sum == 10 ? Throw.spare : .numberOfPins(secondAmount)
                currentThrowSet.append(finalThrow)
            }
        }
    }

    func didTapReset() {
        self.reset()
    }
}
