//
//  AppDelegate.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 23/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        window = UIWindow(frame: UIScreen.main.bounds)
        let coordinator = BowlingGameCoordinator(window: window)
        coordinator.start()
        window?.makeKeyAndVisible()

        return true
    }
}

