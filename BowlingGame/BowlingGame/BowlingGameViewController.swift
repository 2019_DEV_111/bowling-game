//
//  BowlingGameViewController.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 25/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class BowlingGameViewController: UIViewController {

    // MARK: Properties

    private let viewModel: BowlingGameViewModel

    private let titleLabel = UILabel()
    private let selectionLabel = UILabel()
    private let resultLabel = UILabel()

    private let disposeBag = DisposeBag()

    private lazy var bowlingGameTurnCollectionView: UICollectionView = self.createBowlingGameCollectionView()
    private lazy var resetButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.blue, for: .normal)
        button.setTitle("Reset", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 24)
        button.addTarget(self, action: #selector(didTapReset), for: .touchUpInside)
        return button
    }()

    // MARK: Initializers

    init(viewModel: BowlingGameViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)

        setupRx()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Lifecycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        styleViews()
    }

    // MARK: Setup Views

    private func setupViews() {
        view.backgroundColor = .white

        view.addSubview(titleLabel)
        view.addSubview(selectionLabel)
        view.addSubview(resultLabel)
        view.addSubview(bowlingGameTurnCollectionView)
        view.addSubview(resetButton)

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(50)
            make.left.equalToSuperview().offset(20.0)
            make.right.equalToSuperview().offset(-20.0)
            make.height.equalTo(35.0)
        }

        selectionLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(30.0)
            make.left.equalToSuperview().offset(20.0)
            make.right.equalToSuperview().offset(-20.0)
            make.height.equalTo(35.0)
        }

        resultLabel.snp.makeConstraints { make in
            make.bottom.equalTo(bowlingGameTurnCollectionView.snp.top).offset(-20.0)
            make.left.equalToSuperview()
            make.width.equalToSuperview().dividedBy(2.0)
            make.height.equalTo(35.0)
        }

        resetButton.snp.makeConstraints { make in
            make.bottom.equalTo(bowlingGameTurnCollectionView.snp.top).offset(-20.0)
            make.right.equalToSuperview()
            make.width.equalToSuperview().dividedBy(2.0)
            make.height.equalTo(35.0)
        }

        bowlingGameTurnCollectionView.snp.makeConstraints { make in
            make.left.bottom.right.equalToSuperview()
            make.height.equalToSuperview().dividedBy(2.0)
        }
    }

    // MARK: Style

    private func styleViews() {
        titleLabel.font = UIFont.systemFont(ofSize: 32)
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.text = "Bowling Game Scorer"

        selectionLabel.font = UIFont.systemFont(ofSize: 24)
        selectionLabel.textColor = .gray
        selectionLabel.text = "Select"
        selectionLabel.textAlignment = .center

        resetButton.titleLabel?.textAlignment = .center

        resultLabel.font = UIFont.systemFont(ofSize: 24)
        resultLabel.textColor = .black
        resultLabel.textAlignment = .center

        resultLabel.isHidden = viewModel.resultHidden
    }

    // MARK: Custom Methods

    private func createBowlingGameCollectionView() -> UICollectionView {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(BowlingTurnCollectionViewCell.self, forCellWithReuseIdentifier: "bowlingTurnCell")
        collectionView.backgroundColor = .white
        return collectionView
    }

    @objc private func didTapReset() {
        viewModel.didTapReset()
    }

    // MARK: Reactive Code

    private func setupRx() {
        viewModel.resultObservable.asObservable()
            .subscribe(onNext: { [weak self] result in
                guard let `self` = self else { return }
                self.resultLabel.text = "Total: \(result)"
                self.resultLabel.isHidden = self.viewModel.resultHidden
            })
            .disposed(by: disposeBag)

        viewModel.framesObservable.asObservable()
            .subscribe(onNext: { [weak self] frames in
                guard let `self` = self else { return }
                let frameNumber = frames.count
                self.selectionLabel.text = "Frame \(frameNumber) set"
            })
            .disposed(by: disposeBag)
    }

}

// MARK: UICollectionView Methods

extension BowlingGameViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = bowlingGameTurnCollectionView.dequeueReusableCell(withReuseIdentifier: "bowlingTurnCell", for: indexPath) as! BowlingTurnCollectionViewCell
        cell.update(indexPath: indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.userDidSelectCellWith(indexPath: indexPath)
    }
}

extension BowlingGameViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let screenHeight = UIScreen.main.bounds.height
        let cellHeight = ( screenHeight / 8.0) - 10.0
        return CGSize(width: cellHeight, height: cellHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
}
