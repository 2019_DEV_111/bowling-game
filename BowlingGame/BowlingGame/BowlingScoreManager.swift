//
//  BowlingScoreManager.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 24/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import Foundation

let MAX_GAME_SCORE = 300
let MAX_FRAME_SCORE = 30
let MISSEDSTRIKE = 10
let MISSEDSPARE = 10
let STRIKE_BASE_SCORE = 10
let SPARE_BASE_SCORE = 10
let MAX_NUMBER_OF_PINS = 10

enum GameState {
    case start
    case normal
    case spare
    case strike
    case doubleStrike
}

class BowlingScoreManager {

    /*
     Explanation of approach in the returnTotal(forGame:) method:
     Given that we don't want to keep track of intermediate total scores of the bowling game, we don't want an algorythm that simply calculates these subtotals as we add frames. I have found it useful to look at this problem from the other way around:

     Instead of adding up points in order to get to a total general score for the game, we are going to subtract points from the theoretical maximum score for each frame as we go along. A ready-made closure that is built for this specific purpose is REDUCE. It will take a sequence of frames as input, and for each frame it will determine how much needs to be subtracted from the theoretical MAX at every given point. Instead of keeping track of the ACTUALITY of a bowler's points, it will provide the remaining POTENTIALITY of points for a game. And this until we reach the end of the game, and the remaining potentiality for a game's points is reduced to zero.
     */
    func returnTotal(forGame game: BowlingGame) -> Int {
        let frames = game.frames
        var gameState: GameState = .start

        let total = frames.reduce(MAX_GAME_SCORE, { [weak self] nextResult, frame in
            guard let `self` = self, let firstThrow = frame.throwSet.first else { return 0 }

            let totalPins = frame.throwSet.map { self.getTotalPinsFor($0) }.reduce(0, +)
            var toSubtract: Int = 0

            switch gameState {
            case .start, .normal:
                break
            case .spare:
                toSubtract += MAX_NUMBER_OF_PINS - self.getTotalPinsFor(firstThrow)
            case .strike:
                guard !frame.bonus else {
                    let secondThrow = frame.throwSet[1]
                    toSubtract += firstThrow == .strike ?
                        (MAX_NUMBER_OF_PINS - self.getTotalPinsFor(secondThrow)) :
                        MISSEDSTRIKE + (MAX_NUMBER_OF_PINS - self.getTotalPinsFor(secondThrow))
                    break
                }
                if frame.frameState != .strike {
                    toSubtract += frame.frameState == .spare ?
                        MISSEDSTRIKE : MISSEDSPARE + (MAX_NUMBER_OF_PINS - totalPins)
                }
            case .doubleStrike:
                guard !frame.bonus else {
                    let secondThrow = frame.throwSet[1]
                    toSubtract += firstThrow == .strike ?
                        (10 - self.getTotalPinsFor(secondThrow)) :
                        MISSEDSTRIKE + (MAX_NUMBER_OF_PINS - self.getTotalPinsFor(firstThrow) - self.getTotalPinsFor(secondThrow))
                    break
                }
                if frame.frameState != .strike {
                    toSubtract += frame.frameState == .spare ?
                        (MAX_NUMBER_OF_PINS - self.getTotalPinsFor(firstThrow)) + MISSEDSTRIKE :
                        (MAX_NUMBER_OF_PINS - self.getTotalPinsFor(firstThrow)) + MISSEDSPARE + (MAX_NUMBER_OF_PINS - totalPins)
                }
            }

            if frame.bonus {
                toSubtract += (MAX_FRAME_SCORE - getTotalScoreForFrameWithBonus(frame))
            } else {
                switch frame.frameState {
                case .strike:
                    toSubtract += 0
                    gameState = gameState == .strike ? .doubleStrike : .strike
                case .spare:
                    toSubtract += MISSEDSTRIKE
                    gameState = .spare
                case .normal:
                    toSubtract += MISSEDSTRIKE + MISSEDSPARE + (MAX_NUMBER_OF_PINS - totalPins)
                    gameState = .normal
                }
            }

            return nextResult - toSubtract
        })

        return total
    }

    func getTotalPinsFor(_ t: Throw) -> Int {
        switch t {
        case .strike:
            return STRIKE_BASE_SCORE
        case .spare:
            return SPARE_BASE_SCORE
        case .numberOfPins(let total):
            return total
        }
    }

    private func getTotalScoreForFrameWithBonus(_ frame: Frame) -> Int {
        guard frame.bonus, let thirdThrow = frame.throwSet.last else {
            print("passed frame parameter without bonus throw")
            return 0
        }
        let secondThrow = frame.throwSet[1]

        if secondThrow == .spare {
            return SPARE_BASE_SCORE + getTotalPinsFor(thirdThrow)
        } else {
            return STRIKE_BASE_SCORE + getTotalPinsFor(secondThrow) + getTotalPinsFor(thirdThrow)
        }
    }
}
