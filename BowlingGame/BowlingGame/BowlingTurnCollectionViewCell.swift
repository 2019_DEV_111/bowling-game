//
//  BowlingTurnCollectionViewCell.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 25/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import UIKit
import SnapKit

class BowlingTurnCollectionViewCell: UICollectionViewCell {

    // MARK: Properties

    private let displayLabel = UILabel()
    private let values = ["-","1","2","3","4","5","6","7","8","9","X"]

    override var reuseIdentifier: String? {
        return "bowlingTurnCell"
    }

    // MARK: Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        styleViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Layout

    private func styleViews() {
        backgroundColor = .blue
        displayLabel.backgroundColor = .clear
        displayLabel.textColor = .white
        displayLabel.font = UIFont.systemFont(ofSize: 32)
        displayLabel.textAlignment = .center

        addSubview(displayLabel)
        displayLabel.snp.makeConstraints { make in
            make.top.bottom.left.right.equalToSuperview()
        }
    }

    // MARK: Custom Methods

    func update(indexPath: IndexPath) {
        displayLabel.text = values[indexPath.row]
    }
}
