//
//  BowlingGame.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 24/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

enum Throw {
    case numberOfPins(Int)
    case spare
    case strike
}

enum FrameState {
    case normal
    case spare
    case strike
}

struct BowlingGame {
    let frames: [Frame]
}

struct Frame {
    let throwSet: [Throw]

    var bonus: Bool {
        return throwSet.count == 3
    }

    var frameState: FrameState {
        if throwSet.contains(where: { $0 == .strike }) {
            return .strike
        } else if throwSet.contains(where: { $0 == .spare }) {
            return .spare
        } else {
            return .normal
        }
    }
}

extension Throw: Equatable {
    static func ==(lhs: Throw, rhs: Throw) -> Bool {
        switch (lhs, rhs) {
        case (let numberOfPins(total1), let numberOfPins(total2)):
            return total1 == total2
        case (.spare, .spare):
            return true
        case (.strike, .strike):
            return true
        default:
            return false
        }
    }
}


