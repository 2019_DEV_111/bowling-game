//
//  BowlingGameCoordinator.swift
//  BowlingGame
//
//  Created by Kristof Peleman on 25/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
}

final class BowlingGameCoordinator: Coordinator {

    private var window: UIWindow?
    private weak var bowlingGameViewController: BowlingGameViewController? {
        return window?.rootViewController as? BowlingGameViewController
    }

    init(window: UIWindow?) {
        self.window = window
    }

    func start() {
        let viewModel = BowlingGameViewModel()
        let viewController = BowlingGameViewController(viewModel: viewModel)
        window?.rootViewController = viewController
    }
}
