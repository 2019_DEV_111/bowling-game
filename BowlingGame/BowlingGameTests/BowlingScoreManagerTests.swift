//
//  BowlingScoreManagerTests.swift
//  BowlingGameTests
//
//  Created by Kristof Peleman on 25/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import XCTest
@testable import BowlingGame

class BowlingScoreManagerTests: XCTestCase {

    private let bowlingScoreManager = BowlingScoreManager()

    private let zero = Throw.numberOfPins(0)
    private let one = Throw.numberOfPins(1)
    private let two = Throw.numberOfPins(2)
    private let three = Throw.numberOfPins(3)
    private let four = Throw.numberOfPins(4)
    private let five = Throw.numberOfPins(5)
    private let six = Throw.numberOfPins(6)
    private let seven = Throw.numberOfPins(7)
    private let eight = Throw.numberOfPins(8)
    private let nine = Throw.numberOfPins(9)
    private let spare = Throw.spare
    private let strike = Throw.strike

    private var frames: [Frame] = []


    func testReturnTotal_withMaximumScore() {
        let f1 = Frame(throwSet: [strike])
        let f2 = Frame(throwSet: [strike])
        let f3 = Frame(throwSet: [strike])
        let f4 = Frame(throwSet: [strike])
        let f5 = Frame(throwSet: [strike])
        let f6 = Frame(throwSet: [strike])
        let f7 = Frame(throwSet: [strike])
        let f8 = Frame(throwSet: [strike])
        let f9 = Frame(throwSet: [strike])
        let f10 = Frame(throwSet: [strike, strike, strike])

        frames = [f1,f2,f3,f4,f5,f6,f7,f8,f9,f10]
        let game = BowlingGame(frames: frames)

        let expectedResult = 300

        XCTAssertEqual(bowlingScoreManager.returnTotal(forGame: game), expectedResult)
    }

    func testReturnTotal_withTenSpares() {
        let f1 = Frame(throwSet: [five, spare])
        let f2 = Frame(throwSet: [five, spare])
        let f3 = Frame(throwSet: [five, spare])
        let f4 = Frame(throwSet: [five, spare])
        let f5 = Frame(throwSet: [five, spare])
        let f6 = Frame(throwSet: [five, spare])
        let f7 = Frame(throwSet: [five, spare])
        let f8 = Frame(throwSet: [five, spare])
        let f9 = Frame(throwSet: [five, spare])
        let f10 = Frame(throwSet: [five, spare, five])

        frames = [f1,f2,f3,f4,f5,f6,f7,f8,f9,f10]
        let game = BowlingGame(frames: frames)

        let expectedResult = 150

        XCTAssertEqual(bowlingScoreManager.returnTotal(forGame: game), expectedResult)
    }

    func testReturnTotal_withTenThrowsOfNine() {
        let f1 = Frame(throwSet: [four, five])
        let f2 = Frame(throwSet: [four, five])
        let f3 = Frame(throwSet: [four, five])
        let f4 = Frame(throwSet: [four, five])
        let f5 = Frame(throwSet: [four, five])
        let f6 = Frame(throwSet: [four, five])
        let f7 = Frame(throwSet: [four, five])
        let f8 = Frame(throwSet: [four, five])
        let f9 = Frame(throwSet: [four, five])
        let f10 = Frame(throwSet: [four, five])

        frames = [f1,f2,f3,f4,f5,f6,f7,f8,f9,f10]
        let game = BowlingGame(frames: frames)

        let expectedResult = 90

        XCTAssertEqual(bowlingScoreManager.returnTotal(forGame: game), expectedResult)
    }

    func testReturnTotal_withRandomCombinationOfThrows() {
        let firstFrame = Frame(throwSet: [seven, spare])
        let secondFrame = Frame(throwSet: [four, five])
        let thirdFrame = Frame(throwSet: [strike])
        let fourthFrame = Frame(throwSet: [five, spare])
        let fifthFrame = Frame(throwSet: [three, six])
        let sixthFrame = Frame(throwSet: [strike])
        let seventhFrame = Frame(throwSet: [strike])
        let eightFrame = Frame(throwSet: [eight, spare])
        let ninthFrame = Frame(throwSet: [four, three])
        let tenthFrame = Frame(throwSet: [strike, two, eight])

        frames = [
            firstFrame,
            secondFrame,
            thirdFrame,
            fourthFrame,
            fifthFrame,
            sixthFrame,
            seventhFrame,
            eightFrame,
            ninthFrame,
            tenthFrame
        ]
        let game = BowlingGame(frames: frames)

        let expectedResult = 154

        XCTAssertEqual(bowlingScoreManager.returnTotal(forGame: game), expectedResult)
    }

    override func tearDown() {
        frames = []
    }

}
