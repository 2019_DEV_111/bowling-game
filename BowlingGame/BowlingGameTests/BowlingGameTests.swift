//
//  BowlingGameTests.swift
//  BowlingGameTests
//
//  Created by Kristof Peleman on 25/02/19.
//  Copyright © 2019 bnpparibasfortis. All rights reserved.
//

import XCTest
@testable import BowlingGame

class BowlingGameTests: XCTestCase {

    func testBonusShouldBeFalse() {
        let frame_strike = Frame(throwSet: [.strike])
        let frame_spare = Frame(throwSet: [.spare])
        let frame_numberOfPins = Frame(throwSet: [.numberOfPins(5)])

        XCTAssertFalse(frame_strike.bonus)
        XCTAssertFalse(frame_spare.bonus)
        XCTAssertFalse(frame_numberOfPins.bonus)
    }

    func testBonusShouldBeTrue() {
        let bonusFrameWithSpare = Frame(throwSet: [.numberOfPins(6), .spare, .numberOfPins(8)])
        let bonusFrameWithStrike = Frame(throwSet: [.strike, .numberOfPins(9), .numberOfPins(0)])
        let bonusFrameWithAllStrikes = Frame(throwSet: [.strike, .strike, .strike])

        XCTAssertTrue(bonusFrameWithSpare.bonus)
        XCTAssertTrue(bonusFrameWithStrike.bonus)
        XCTAssertTrue(bonusFrameWithAllStrikes.bonus)
    }

    func testFrameState_strike() {
        let expectedResult = FrameState.strike
        let frame_strike = Frame(throwSet: [.strike])

        XCTAssertEqual(frame_strike.frameState, expectedResult)
    }

    func testFrameState_spare() {
        let expectedResult = FrameState.spare
        let frame_spare = Frame(throwSet: [.spare])

        XCTAssertEqual(frame_spare.frameState, expectedResult)
    }

    func testFrameState_numberOfPins() {
        let expectedResult = FrameState.normal
        let frame_numberOfPins = Frame(throwSet: [.numberOfPins(9)])

        XCTAssertEqual(frame_numberOfPins.frameState, expectedResult)
    }


}
