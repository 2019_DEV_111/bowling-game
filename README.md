# Bowling Game

HOW TO INSTALL

1. Clone bitbucket repo into local folder on a macbook
2. In terminal, navigate to the folder containing PodFile
3. Type: pod install & press ENTER (this will install the SnapKit and RxSwift library as dependencies into your local copy)
4. Close Xcode (if it was running at this point)
5. Open the project in Xcode via the .xcworkspace file
6. Run the project on an iOS simulator with iOS version 11.1 or later


HOW TO USE

Tap the blue cells on the bottom half of the view to select a number of pins to throw down. For every 2 (or 3 in case of bonus) selections, a new frame is added to the game.
Once 10 full frames have been completed, the total score will be displayed on screen.

Tap reset to start over.